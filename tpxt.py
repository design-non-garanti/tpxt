# coding: utf-8

import time
import sys
import json
from PIL import Image



args=sys.argv
def text_to_px():
	with open('settings.json', 'r') as jsn:
		settings = json.load(jsn)

	for a in range(1,len(args)-2):

		lines=[]
		xSize=0

		with open(args[a], encoding='utf-8') as f:
			for line in f:
				lines.append([])
				for char in line:
					if(char == '\t'):
						lines[len(lines)-1].append(settings[' '])
						lines[len(lines)-1].append(settings[' '])
						lines[len(lines)-1].append(settings[' '])
						lines[len(lines)-1].append(settings[' '])
						continue
					if(char == '\n'):
						continue
					if char in settings:
						lines[len(lines)-1].append(settings[char])
					else:
						lines[len(lines)-1].append((255,255,255,255))
				if len(lines[len(lines)-1])>xSize:
					xSize=len(lines[len(lines)-1])
				for ratio in range(0,int(args[len(args)-2])-1):
					lines.append(lines[len(lines)-1])

		data=[]

		for line in lines:
			while len(line) < xSize:
				line.append((128,128,128,0))
			for char in line:
				for ratio in range(0,int(args[len(args)-2])):
					data.append(tuple(char))

		img = Image.new('RGBA', (xSize*int(args[len(args)-2]),len(lines)))
		img.putdata(data)
		img.save(args[len(args)-1]+".gif", "PNG")

		with open(args[a], encoding='utf-8') as f:
			past_input=f.read()

		while True:
			time.sleep(1)
			with open(args[a], encoding='utf-8') as f:
				new_input=f.read()

			if(past_input != new_input):
				break

		text_to_px()

text_to_px()